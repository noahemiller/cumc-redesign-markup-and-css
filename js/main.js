
    $(function() {
        
       
            $('.globalfooter > div').columnize({width: 200});
   
            $(".globalheader ul li").hide();    
            $(".globalNavToggle").hide();
            $(".globalNavToggle.open").show();
            $(".globalNavToggle").click(function() {
                
                $( ".globalNavToggle.open" ).hide();
                $( ".globalNavToggle" ).toggleClass('open');
                $( ".globalNavToggle.open" ).toggle("slide");
                $( ".globalheader ul li" ).toggle("slide");
            });

        
  
  /* Subnav selector - used on the moodboard */
        
  /*  

These are some prototypes we're not using right now. maybe we'll return to them later, so I'm leaving these switches in. 

      $(".standardnav").click(function() {
            $('body').attr('class','standard-subnav');
            $('.localnav').columnize();
            $(window).resize();
              });
        $(".verticalnav").click(function() {
            $('body').attr('class','vertical-subnav');
            $(window).resize();
              });
*/

       

        $(".slideoutnav").click(function() {
            $('body').attr('class','slideout-subnav');
            $(window).resize();
              });
  
  
 /* Stickydiv */

        var s = $(".drawershow");
            var pos = s.position();
            $(window).scroll(function() {
                var windowpos = $(window).scrollTop();
                if (windowpos >= pos.top) {
                    s.addClass("stickdiv ");
                } else {
                    s.removeClass("stickdiv ");
                }
            });

  /* Makes the slideout/drawer subnav work */

		/* This part makes the drawer close after 1500ms 

        $(".localnav ul").mouseleave(function(){
            navdrawerTimeout = setTimeout(function(){
                    $(".localnav ul").hide('slide');
                    $(".drawershow").show('slide');
                },1500);
                $(".localnav ul").mouseenter(function(){
                clearTimeout(navdrawerTimeout);
                });
            }); 
*/

        $(".drawertoggle").click(function() {
            $('.localnav ul').toggle('slide');
            $('.drawertoggle').toggle();
            clearTimeout(navdrawerTimeout);

              });
        $(".drawerhide").click(function() {
            $('.localnav ul').hide('slide');
            $(".drawershow").show('slide');
            clearTimeout(navdrawerTimeout);
              });

        $("body.localnavopen *").click(function() {
            $('.localnav ul').hide();
            $(".drawershow").show('slide');
            clearTimeout(navdrawerTimeout);
              });
  /* Parallax Background experiment */


        $(document).ready(function(){
            $('section[data-type="background"]').each(function(){
                var $bgobj = $(this); // assigning the object
             
                $(window).scroll(function() {
                    var yPos = -($window.scrollTop() / $bgobj.data('speed')); 
                     
                    // Put together our final background position
                    var coords = '50% '+ yPos + 'px';
         
                    // Move the background
                    $bgobj.css({ backgroundPosition: coords });
                }); 
            });    
        });

  /* Accordion for Global Footer */


        $("#accordion").addClass("great-js-is-on");
        $("#accordion h3").next().hide();
        $("#accordion h3").click(function(){
            if (!$(this).hasClass('active')) {
                $(".active").removeClass("active");
                $( this ).addClass("active");
                $(".open").removeClass("open").slideUp("fast");
                $( this ).next("div").addClass("open");
                $(".open").slideToggle("slow");
                $(".globalfooterbasics").hide();
                }
             else {
                $(".active").removeClass("active");
                $(".open").removeClass("open").slideUp("fast");
                $(".globalfooterbasics").show();
			}
        });

  /* Toggle to show active button states */

        $(".button").click(function() {
            $(this).toggleClass("active");
        });
  
  /* Basic Nav Active Toggle */

        $("nav.basic li").click(function() {
            $("nav.basic li").removeClass("active");
            $(this).toggleClass("active");
        });

  /* Sets states/context for various Features */
      
      /* Faculty */
        $(".faculty nav.basic li.standard").click(function() {
            $(".facultyindex").attr('class','facultyindex');
        });
        $(".faculty nav.basic li.abbreviated-horizontal").click(function() {
            $(".facultyindex").attr('class','facultyindex abbreviated');
        });
        $(".faculty nav.basic li.abbreviated-vertical").click(function() {
            $(".facultyindex").attr('class','facultyindex abbreviated aside');
        });

      /* News */

        $(".news nav.basic li.standard").click(function() {
            $(".news-index").attr('class','news-index');
        });
        $(".news nav.basic li.horizontal").click(function() {
            $(".news-index").attr('class','news-index lateral');
        });
        $(".news nav.basic li.abbreviated").click(function() {
            $(".news-index").attr('class','news-index abbreviated');
        });
        $(".news nav.basic li.abbreviated-horizontal").click(function() {
            $(".news-index").attr('class','news-index abbreviated lateral');
        });
        $(".news nav.basic li.abbreviated-aside").click(function() {
            $(".news-index").attr('class','news-index abbreviated aside');
        });

      /* Events */

         $(".events nav.basic li.standard").click(function() {
            $(".events-index").attr('class','events-index');
        });
        $(".events nav.basic li.horizontal").click(function() {
            $(".events-index").attr('class','events-index lateral');
        });
        $(".events nav.basic li.abbreviated").click(function() {
            $(".events-index").attr('class','events-index abbreviated');
        });
        $(".events nav.basic li.abbreviated-horizontal").click(function() {
            $(".events-index").attr('class','events-index abbreviated lateral');
        });
        $(".events nav.basic li.aside").click(function() {
            $(".events-index").attr('class','events-index aside');
        });
        $(".events nav.basic li.abbreviated-aside").click(function() {
            $(".events-index").attr('class','events-index abbreviated aside');
        });

  /* Jquery UI Stuff from here down */


        $( "#autocomplete" ).autocomplete({
        });
        

        
        $( "#button" ).button();
        $( "#radioset" ).buttonset();
        

        $( "#tabs" ).tabs({  });
        $( "#tabs" ).tabs({collapsible: true, active: false, hide: { effect: "fade", duration: 300 },show: { effect: "fade", duration: 300 } });

        
        $( "#datepicker" ).datepicker({
            inline: true
        });
        

        
        $( "#slider" ).slider({
            range: true,
            values: [ 17, 67 ]
        });
        

        
        $( "#progressbar" ).progressbar({
            value: 20
        });
        

        // Hover states on the static widgets
        $( "#dialog-link, #icons li" ).hover(
            function() {
                $( this ).addClass( "ui-state-hover" );
            },
            function() {
                $( this ).removeClass( "ui-state-hover" );
            }
        );
    });
